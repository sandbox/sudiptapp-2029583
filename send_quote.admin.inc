<?php

/**
 * @file
 * Admin page functionality for the send_quote module.
 */

/**
 * List of the Send Quotes Start.
 */
function send_quote_list() {
  $result = db_query('SELECT * FROM {send_quote} ORDER BY id DESC');
  $rows = array();
  while ($quote = db_fetch_object($result)) {
    $rows[] = array(
      check_plain($quote->full_name),
      check_plain($quote->email_id),
      ($quote->status ? t('Yes') : t('No')),
      l(t('edit'),
      'admin/build/send_quote/edit/' . $quote->id),
      l(t('delete'),
      'admin/build/send_quote/delete/' . $quote->id),
    );
  }
  $header = array(
    t('Full Name'),
    t('Email'),
    t('Sent Reply'),
    array(
      'data' => t('Operations'),
      'colspan' => 2),
  );

  return theme('table', $header, $rows);
}
/* List of the Send Quotes End */

/**
 * Add or Edit form Start.
 */
function send_quote_admin_edit($op, $form_state = array(), $send_quote = NULL) {
  if (empty($send_quote) || $op == 'add') {
    $send_quote = array(
      'full_name' => '',
      'email_id' => '',
      'message' => '',
      'status' => 0,
      'date' => '0000-00-00 00:00:00',
      'id' => NULL,
    );
  }
  $form['send_quote_op'] = array('#type' => 'value', '#value' => $op);
  $form['full_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name'),
    '#maxlength' => 255,
    '#default_value' => $send_quote['full_name'],
    '#description' => t("Example: 'John Smith' or 'David Brown'."),
    '#required' => TRUE,
  );
  $form['email_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Email ID'),
    '#maxlength' => 255,
    '#default_value' => $send_quote['email_id'],
    '#description' => t("Example: 'info@example.com' or 'sales@example.com,support@example.com'."),
    '#required' => TRUE,
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $send_quote['message'],
    '#description' => t('Description of your Quote.'),
  );
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Sent Reply'),
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => $send_quote['status'],
    '#description' => t('Set this to <em>Yes</em> if you already sent the reply for this quote.'),
  );
  $form['date'] = array(
    '#type' => 'value',
    '#value' => $send_quote['date'],
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $send_quote['id'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}
/* Add or Edit Form End */

/**
 * Edit form action Start.
 */
function send_quote_admin_edit_submit($form, &$form_state) {
  if (empty($form_state['values']['id']) || $form_state['values']['send_quote_op'] == 'add') {
    drupal_write_record('send_quote', $form_state['values']);
    drupal_set_message(t('Quote %full_name has been added.', array('%full_name' => $form_state['values']['full_name'])));

  }
  else {
    drupal_write_record('send_quote', $form_state['values'], 'id');
    drupal_set_message(t('Quote %full_name has been updated.', array('%full_name' => $form_state['values']['full_name'])));
  }

  $form_state['redirect'] = 'admin/build/send_quote/list';
}
/**
 * Edit form action End.
 */

/**
 * Validate Add or Edit form Start.
 */
function send_quote_admin_edit_validate($form, &$form_state) {
  if (empty($form_state['values']['full_name'])) {
    form_set_error('full_name', t('You must enter your  Full Name.'));
  }
  if (empty($form_state['values']['email_id'])) {
    form_set_error('email_id', t('You must an Email ID.'));
  }
  else {
    if (!valid_email_address(trim($form_state['values']['email_id']))) {
      form_set_error('email_id', t('%email_id is an invalid e-mail address.', array('%email_id' => $form_state['values']['email_id'])));
    }
  }
}
/* Validate Add/Edit Form End */

/**
 * Delete quote confirmation Start.
 */
function send_quote_admin_delete(&$form_state, $send_quote) {

  $form['send_quote'] = array(
    '#type' => 'value',
    '#value' => $send_quote,
  );

  return confirm_form($form, t('Are you sure you want to delete the quote of %full_name person?', array('%full_name' => $send_quote['full_name'])), 'admin/build/send_quote/list', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}
/* Delete Quote Confirmation End */

/**
 * Delete quote Start.
 */
function send_quote_admin_delete_submit($form, &$form_state) {
  $send_quote = $form_state['values']['send_quote'];
  db_query("DELETE FROM {send_quote} WHERE id = %d", $send_quote['id']);
  drupal_set_message(t('Quote of %full_name person has been deleted.', array('%full_name' => $send_quote['full_name'])));

  $form_state['redirect'] = 'admin/build/send_quote/list';
}
/* Delete Quote  End */
