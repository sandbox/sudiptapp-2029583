Drupal.behaviors.send_quote = function(context) {
function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if(!emailReg.test($email)) {
    return false;
  } else {
    return true;
  }
}
 $("#edit-full-name").focus(function()
 {
  if($(this).val() == "Type your full name")
  {
    $(this).val("");
  }
 });
 $("#edit-full-name").blur(function()
 {
  if($(this).val() == "")
  {
    $(this).val("Type your full name");
  }
 });
 $("#edit-email-id").focus(function()
 {
  if($(this).val() == "Type your email id")
  {
  $(this).val("");
  }
 });
 $("#edit-email-id").blur(function()
 {
  if($(this).val() == "")
  {
  $(this).val("Type your email id");
  }
 });
 $("#edit-message").focus(function()
 {
  if($(this).val() == "Type your query")
  {
  $(this).val("");
  }
 });
 $("#edit-message").blur(function()
 {
  if($(this).val() == "")
  {
  $(this).val("Type your query");
  }
 });
 $("#edit-submit-1").click(function()
 {
  var errMsg = '';
  var errMsg1 = "";
  var errMsg2 = "";
  var errMsg3 = "";
  if($("#edit-full-name").val() == '' || $("#edit-full-name").val() == 'Type your full name')
  {
  errMsg1 = "First Name should not be blank";
  $("#errMsgSendQuote").html(errMsg1);
  return false;
  }
  if($("#edit-email-id").val() == '' || $("#edit-email-id").val() == 'Type your email id')
  {
  errMsg2 = "Email should not be blank";
  $("#errMsgSendQuote").html(errMsg2);
  return false;
  }
  if(!validateEmail($("#edit-email-id").val()))
  {
  errMsg2 = "Invalid Email";
  $("#errMsgSendQuote").html(errMsg2);
  return false;
  }
  if($("#edit-message").val() == '' || $("#edit-message").val() == 'Type your query')
  {
  errMsg3 = "Message should not be blank";
  $("#errMsgSendQuote").html(errMsg3);
  return false;
  }
  return true;
 });
}
