<?php

/**
 * @file
 * Front page functionality for the send_quote module.
 */

/**
 * Display form of the Send Quotes Start.
 */
function send_quote_front_page() {
  return drupal_get_form('front_page_blockform');
}
