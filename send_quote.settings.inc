<?php

/**
 * @file
 * Setting page functionality for the send_quote module.
 */

/**
 * Settings for send quote Start.
 */
function send_quote_manage_admin() {
  $form = array();
  $form['send_quote_send_email'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#description' => t('Users quotation comes to this email id'),
    '#default_value' => variable_get('send_quote_send_email', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
