This module allows users to post their query/quotation 
from every page to the administrator. This form is working
as a block. Administrator can set it on footer of every 
page and also use it for one page only like a contact us
page. Once users submitted their query/quotation
through this form, an email goes to the administrator 
with the inputted details of users and also this 
information store into the database.

Administrator can be able to see the list of quotations.
He can be able to edit/delete the quotations.He can also be
able add the quotations from his panel when this will be 
required. Also, there is an option for admin to change status 
of the quotation after sent the reply for that quotation.

Administrator must be set the email id from the setting page
of this module where the notification email will be send once
user send his/her quotation.
